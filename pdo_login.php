<?php  
 session_start();  
 $host = "localhost";  
 $username = "helene";  
 $password = "BEURY";  

 $message = "";  
 try  
 {  
      $connect = new PDO("mysql:host=$host; dbname=utilisateurs", $username, $password);  
      $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      if(isset($_POST["login"]))  
      {  
           if(empty($_POST["username"]) || empty($_POST["password"]))  
           {  
                $message = '<label class="erreur">Tous les champs doivent être remplis</label>';  
           }  
           else  
           {  
                $query = "SELECT * FROM mesutilisateurs WHERE nom = :username AND mdp = :password";  
                $statement = $connect->prepare($query);  
                $statement->execute(  
                     array(  
                        'username'     =>     $_POST["username"],  
                        'password'     =>     $_POST["password"]  
                     )  
                );  
                $count = $statement->rowCount();  
                if($count > 0)  
                {  
                     $_SESSION["username"] = $_POST["username"];  
                     header("location:login_success.php");  
                }  
                else  
                {  
                     $message = '<label class="erreur"> données incorrectes </label>';  
                }  
           }  
      }  
 }  
 catch(PDOException $error)  
 {  
      $message = $error->getMessage();  
 }  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  <link rel="stylesheet" href="style.css">

           <title>exo php</title>  
           
            
      </head>  
      <body>  
</br>
           
                <?php  
                if(isset($message))  
                {  
                     echo '<label class="text-danger">'.$message.'</label>';  
                }  
                ?>  
                <div class=mapage>
                <h3>Formulaire de connexion</h3>  
                <form  method="post">  
                     <label>Nom d'utilisateur</label>  
                     <input type="text" name="username" class="form-control" />  
                     </br>
                     <label>Mot de passe</label>  
                     <input type="password" name="password" class="form-control" />  
                     </br>
                     <input type="submit" name="login" class="btn" value="connexion" />  
                </form>  
               </div>
          
            
      </body>  
 </html>  

